//
//  ViewController.swift
//  expandingRow
//
//  Created by Kevin Veverka on 5/3/19.
//  Copyright © 2019 Kevin Veverka. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, cellProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TableViewCell {
            cell.delegate = self
            return cell
        } else {
            return TableViewCell()
        }
    }
    
    func updateRowHeight() {
        self.tableView.beginUpdates()
        self.tableView.endUpdates() 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
}

