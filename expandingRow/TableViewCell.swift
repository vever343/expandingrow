//
//  TableViewCell.swift
//  expandingRow
//
//  Created by Kevin Veverka on 5/3/19.
//  Copyright © 2019 Kevin Veverka. All rights reserved.
//

import UIKit

protocol cellProtocol {
    func updateRowHeight()
}

class TableViewCell: UITableViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var ratingLineView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    var delegate:MainVC?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.isHidden = true
    }

    @IBAction func commentButtonPressed(_ sender: Any) {
        if textView.isHidden {
            commentButton.setTitle("-", for: .normal)
        } else {
            commentButton.setTitle("+", for: .normal)
        }
        textView.isHidden = !textView.isHidden
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
            self.delegate?.updateRowHeight()
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
        if !descLabel.isHidden {
            deleteButton.setTitle("Undo", for: .normal)
            descLabel.isHidden = true
            ratingLineView.isHidden = true
            textView.isHidden = true
        } else {
            deleteButton.setTitle("Delete", for: .normal)
            descLabel.isHidden = false
            ratingLineView.isHidden = false
            textView.isHidden = false
        }
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
            self.delegate?.updateRowHeight()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
    }
}
